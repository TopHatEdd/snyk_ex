# snyk_ex

HTTP API exercise for `Snyk`.  
Worked in 2 sessions. The initial commit included 3 hours of work. 
The second one has a commit message "session start" to measure elapsed time.

## Done
* Implemention.  
* Has docstring in most (all?) functions and methods.  
* Tests of core functionality.
* Uses `Redis` for caching and easy future growth to multiple containers. 
This is an alternative to networking foreign processes and share a single key 
store at the `Python` level. I tried implementing something like that once and 
it was challenging, to say the least.  
* Uses `Redis` for memory limitation so we don't swallow said millions of
packages into our own memory. It is set to remove oldest key when memory is
depeleted. Also, let's us ride on `Redis`' built-in sharding functionality if 
we ever need it.  
* Has `Dockerfile` for our app.  
* Has a docker compose `yaml` for our "product".  
* Uses `gunicorn` + `FastAPI` HTTP framework for easy, production ready, async 
HTTP API implementation.  
* Uses `FastAPI` to generate `OpenAPI` (formerly `Swagger`) docs. It allows for
 easy integration and greater visibility into our backend while keeping costs 
down.  
* Has two GET methods. One for "cache only" (for, maybe, onkeydown event, 
online search in a UI) and another for searching for cache and remote NPM 
repository.  
  
## Missing
* A few more tests for FastAPI.  
* A simple CI that'll build our image and push it when all tests are successful 
because why not. It's pretty much copy paste off previous projects = cheap. 
* Redis disk storage persistancy (can sync/recover from disk etc) by mounting
whatever is configured in the `.env` file. I'd wager this is a little overkill
as we'll probably never run our service using docker-compose or docker-swarm
and, thus, never use this `yaml` more than a POC. Which means, we can, 
theoretically, let the data blow up and when we use an orchastretor, such as
`k8s`, we'll configure `Redis` with something like `Helm`. On the plus side,
leaving this configuration here will at least "draw attention" to whoever is 
reading/using our POC that we want to save our cache to disk for better UX on 
recovery. Either way, if we're already talking about it, this is transparent 
to the app.
  
## Notes
### Redis as LRU
Redis provides a simple solution for our use case to store data limited by
time and size with simple types. To turn this "feature" on, we only need to 
set `maxmemory` and a key eviction policy in `redis.conf`. In our case, delete
the oldest key when we've reached our memory limit.
> [source](https://redis.io/topics/lru-cache)

### Async and gunicorn
`gunicorn` is a solid production ready solution for a Python HTTP backend which
, among other things, enables a smooth UX experience by not blocking each
request.  
```
Gunicorn 'Green Unicorn' is a Python WSGI HTTP Server for UNIX. It's a pre-fork worker model. The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly speedy.
```
> [source](https://gunicorn.org/)
It creates a few workers threads that'll handle the HTTP requests and spare
us from re-inventing the wheel while minimizing errors on this front. Common
tools like this are, usually, already well implemented and we should embrace
them.  
Although we can set the log file destination using an argument to `gunicorn`,
we should also consider using `docker`'s built in driver. It supports common
scenarios such as outputting to `syslog`, `json` and others 
[source](https://docs.docker.com/config/containers/logging/configure/). Which 
means, in the future, it'll be easier to hook it up to a monitoring/alert
solution if we get used to using the driver now. To do that, we'll let our
processes "dump" their output to stdout and let `docker` handle the log files.

### Containerization and scaling
Although I just focued on unit tests, the app is containerized and written
to enable horizontal scaling. In essence, once the need arises, we just need
to deploy Redis under an orchestrator. This change will be transparent to our
app. At the Redis level, we'll choose a strategy for replication/sharding/
slaves/masters/persistancy. This allows our app to be resilient to restarts
and flexible to replication. We can add as many container instances to our
host/cluster and its behavior will not change. For load balancing, we
do that with something like `HAProxy` that'll route requests to multiple
instances of `gunicorn` with something as simple as a config file. It is
important to note that both `HAProxy` and `Redis` have a maintained Docker
image (and a `Helm` chart, for `Redis` that'll be useful if we go big) which
reduces our deployment work and keeps production stable.

### Docker compose
I didn't test it (used it plenty before and time has run out) but all we need 
is
```
docker-compose up
```
The dependency between our code and `Redis` enforce the start up sequence and
avoid false availablility. A few more items that we can add are using our own
docker registry, if we need a private one, and a TLS termination container. 
Afterwards, a "cheap and temporary" solution for a single host deployment 
is creating a `systemd` service unit that'll enforce waiting for the `docker`
service before our product service comes up. Then, `docker-compose` will be
responsible for our container[s] failover while `systemd` for `docker`+product
failover.
