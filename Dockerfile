FROM python:3.8-alpine as base

LABEL inspiration="Sebastian Ramirez <tiangolo@gmail.com>"

COPY requirements.txt .

RUN apk add --no-cache --virtual .build-deps gcc libc-dev make \
    && pip install -r requirements.txt \
    && apk del .build-deps gcc libc-dev make

FROM python:3.8-alpine

COPY --from=base /usr/local /usr/local

# Gunicron related scripts
COPY ./start.sh /start.sh
COPY ./gunicorn_conf.py /gunicorn_conf.py
COPY ./start-reload.sh /start-reload.sh
RUN chmod +x /start.sh
RUN chmod +x /start-reload.sh

# Actual code
COPY app /app/app
# COPY tests /app/tests
WORKDIR /app

# gunicron_conf.py uses more ENV vars to configure gunicorn
ENV PYTHONPATH=/app
# Our configurations
ENV REDIS_HOST="redis"
ENV REDIS_PORT=6379
# Listening address
ENV HOST=0.0.0.0
ENV PORT=9030
EXPOSE ${PORT}

# Run the start script, it will check for an /routers/prestart.sh script (e.g. for migrations)
# And then will start Gunicorn with Uvicorn
ENTRYPOINT ["/start.sh"]
