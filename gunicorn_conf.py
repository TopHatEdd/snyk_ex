#!/usr/bin/env python3
import json
import multiprocessing
import os

# https://pythonspeed.com/articles/gunicorn-in-docker/

workers_per_core_str = os.getenv("WORKERS_PER_CORE", "1")
web_concurrency_str = os.getenv("WEB_CONCURRENCY", None)
host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "80")
bind_env = os.getenv("BIND", None)
use_loglevel = os.getenv("LOG_LEVEL", "INFO")
use_log_file = os.getenv("LOG_FILE", "-")
if bind_env:
    use_bind = bind_env
else:
    use_bind = f"{host}:{port}"

cores = multiprocessing.cpu_count()
workers_per_core = float(workers_per_core_str)
default_web_concurrency = workers_per_core * cores
if web_concurrency_str:
    web_concurrency = int(web_concurrency_str)
    assert web_concurrency > 0
else:
    web_concurrency = max(int(default_web_concurrency), 2)

# Gunicorn config variables
loglevel = use_loglevel
workers = web_concurrency
threads = 2
bind = use_bind
keepalive = 120
timeout = 600
graceful_timeout = 60
worker_class = 'uvicorn.workers.UvicornWorker'
worker_tmp_dir = '/dev/shm'

errorlog = use_log_file
# Away from group to avoid using if
# Best workaround we found for gunicorn open bug
# with a suggestion to reproduce at
# https://github.com/benoitc/gunicorn/issues/1801#issuecomment-446091960
threads = os.getenv("THREADS", web_concurrency)

# For debugging and testing
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "threads": threads,
    "bind": bind,
    "timeout": timeout,
    "graceful_timeout": graceful_timeout,
    "workers_per_core": workers_per_core,
    "worker_class": worker_class,
    # Additional, non-gunicorn variables
    "host": host,
    "port": port,
}
print(json.dumps(log_data))
