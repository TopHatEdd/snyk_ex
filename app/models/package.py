#!/usr/bin/env python3
"""
Contains our model for package x version.
More of a best practice.
"""
import json
from pydantic import BaseModel


class Package(BaseModel):
    """
    Expected structure when dealing with package queries input/output.
    """
    name: str = None
    version: str = None

    def __str__(self):
        return f"{self.name}-{self.version}"
