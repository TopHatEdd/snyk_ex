#!/usr/bin/env python3
"""
NPM package dependncy tree builder functions.
These are the building blocks that allow fetching dependencies from
our own cache, or the remote NPM API if it's missing, for a dependency tree.
"""
import requests
from typing import List
from urllib.parse import urljoin
from app import utils, cache_db, models

DB = cache_db.DB()


def dependency_tree(package: models.Package, cached_only: bool=False) -> dict:
    """
    Builds the depedency tree for a given package and returns it.
    Written as a recursion with stop condition being package without
    any dependencies (the leaf of the tree).
    Anything in between is considered a node and will return the rest.
    """
    deps = find_dependencies(package, cached_only)
    if not deps:
        return {str(package): {}}

    sub_tree = {}
    for dep in deps:
        sub_tree.update(dependency_tree(dep, cached_only))

    return {str(package): sub_tree}


def find_dependencies(package: models.Package,
                      cached_only: bool) -> List[models.Package]:
    """
    Returns a given package's dependencies by searching our cache, first,
    and then the remote NPM repository, if it's missing.
    """
    utils.log.debug(f"Looking for {package} dependencies.")
    package_json = utils.package2json(package)
    try:
        found = [utils.json2package(v) for v in DB.read(package_json)]
        return found
    except KeyError:
        if cached_only:
            msg = "Using cache only, {package} dependencies not found."
            utils.log.info(msg)
            return []
        utils.log.debug(f"Fetching and caching {package} dependencies.")
        deps = npm_repository(package)
        DB.write(key=package_json,
                 dependencies=[utils.package2json(dep) for dep in deps])
        return deps


def npm_repository(package: models.Package) -> List[models.Package]:
    """
    Ask, and parse, the remote NPM repo for a given package's dependencies.
    """
    res = npm_request(package)
    return utils.response2packages(res)


def npm_request(package: models.Package) -> requests.models.Response:
    """
    Asks the remote NPM repository for a given package's dependencies.
    """
    # Example https://registry.npmjs.org/async/2.0.1
    # Returns code 404 when version doesn't exist. Includes JSON data of
    # version.
    # Returns code 404 when package doesn't exist. Includes generic JSON.
    # Returns code 200, if found.
    url = utils.get_config("NPM_REPO_URL")
    url = urljoin(url, urljoin(package.name + '/', package.version))
    res = requests.get(url)

    if res.status_code != 200:
        raise KeyError(f"{package} not found in NPM repository")
    else:
        return res
