#!/usr/bin/env python3
"""
Utils for our app.
Log, configuration and package input/output adapter.
"""
import os
import logging
import json
import requests
from typing import List
from app import models

# Log to the same location our HTTP handler, gunicorn, logs to.
log = logging.getLogger('gunicorn.error')


def get_config(key: str) -> str:
    """
    Return a configuration value. Read from the environ.
    """
    return os.environ[key]


def package2json(package: models.Package) -> str:
    """
    Convert a Package to json.
    """
    # Future proofing
    return package.json()


def json2package(raw_json: str) -> models.Package:
    """
    Convert a raw JSON to Package.
    """
    # Using BaseModel type to avoid importing from app.
    p_json = json.loads(raw_json)
    return models.Package(name=p_json['name'], version=p_json['version'])


def response2packages(response: requests.models.Response) -> List[models.Package]:
    """
    Convert a dependencies response from the NPM repository to a list of Packages.
    """
    # Response is expected as JSON. Notable keys are "name"
    # and "dependencies". The latter being key value of name: version.
    dependencies = response.json()['dependencies'].items()
    return [models.Package(name=key, version=value)
            for key, value in dependencies]
