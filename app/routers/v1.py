#!/usr/bin/env python3
"""
Containers the V1 endpoints for our NPM dependency tree resolver.
Has two GET methods. One only asking the cache. Another asking cache+NPM repo.
"""
from fastapi import APIRouter
from app import utils, npm_dependencies, models

router = APIRouter()
log = utils.log


@router.get("/v1/cached_packages")
def get_cached_package(package: models.Package):
    """
    Query the cache only for given package name.
    """
    return npm_dependencies.dependency_tree(package, cached_only=True)


@router.get("/v1/packages")
def get_package(package_name: models.Package):
    """
    Query the backend for a given package name's dependencies.
    """
    return npm_dependencies.dependency_tree(package, cached_only=False)
