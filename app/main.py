#!/usr/bin/env python3
"""
NPM dependency tree API.
"""
from fastapi import FastAPI
from app import routers
from app import cache_db

app = FastAPI()  # pylint: disable=invalid-name
app.include_router(routers.v1.router, prefix='/v1')


@app.on_event("shutdown")
async def shutdown():
    """
    teardown the application
    """
    cache_db.DB().close()
