#!/usr/bin/env python3
"""
Our App's cache DB to help us use a cluster wide LRU.
Supports read and write of a key: list.
Uses Redis, currently.
"""
import redis
from typing import List
from app import utils


class DB:
    """
    Container class for our cache DB with support for lists only.
    """
    def __init__(self,
                 redis_host: str=None,
                 redis_port: int=None,
                 expiration_sec: int=None,
                 *args,
                 **kwargs):
        if redis_host is None:
            redis_host = utils.get_config('REDIS_HOST')
        if redis_port is None:
            redis_port = utils.get_config('REDIS_PORT')
        if expiration_sec is None:
            expiration_sec = utils.get_config('KEY_EXPIRATION_SEC')

        self.redis_host = redis_host
        self.redis_port = redis_port
        self.expiration_sec = expiration_sec
        self._conn = None

    @property
    def conn(self):
        """
        Lazy connection to our Redis DB.
        """
        if self._conn is None:
            self._conn = redis.Redis(host=self.redis_host,
                                     port=self.redis_port)
        return self._conn

    def close(self):
        try:
            self._conn.close()
        except RedisError as err:
            utils.log.exception(f"Unexpected error closing Redis {err}")

    def read(self, key: str) -> List[str]:
        """
        Returns a list for a key, if found. Raises KeyError if it doesn't exist.
        """
        if not self.conn.exists(key):
            raise KeyError(key)

        key_set = self.conn.smembers(key)
        return [value.decode('utf-8') for value in key_set]

    def write(self, key: str, values: List[str], expires: bool=True):
        """
        Writes values to a given key.
        """
        is_new = self.conn.exists(key)
        values = [v.encode('utf-8') for v in values]
        self.conn.sadd(key, *values)
        if expires and is_new:
            self.conn.expire(key, self.expiration_sec)
