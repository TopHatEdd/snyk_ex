#!/usr/bin/env python3
"""
Tests for the various functions under npm_dependencies.
"""
import responses
import requests
import pytest
from app import npm_dependencies, models, utils


@pytest.fixture(scope='module')
def packages():
    p0 = models.Package(name='p0', version='1.0')
    p1 = models.Package(name='p1', version='1.0')
    p2 = models.Package(name='p2', version='1.0')
    p3 = models.Package(name='p3', version='1.0')
    p4 = models.Package(name='p4', version='1.0')
    return p0, p1, p2, p3, p4


@pytest.fixture(scope='module')
def fake_db(packages):
    """
    Set up our fake DB once for any multiple use.
    """
    # Our fake DB was getting rather large so moved it outside.
    class FakeDB:
        def read(self, package: str) -> list:
            """
            Mock our cache with some set values.
            """
            try:
                return dependencies[package]
            except KeyError:
                return p0

    p0, p1, p2, p3, p4 = map(utils.package2json, packages)
    dependencies = {p0: [p1],
                    p1: [p2, p3],
                    p2: [p4],
                    p3: [],
                    p4: []}

    return FakeDB()


def test_dependency_tree(packages, fake_db, monkeypatch):
    monkeypatch.setattr(npm_dependencies, 'DB', fake_db)
    p0, *_ = packages
    expected = {str(p0):
                    {'p1-1.0':
                        {'p2-1.0': {
                            'p4-1.0': {}},
                         'p3-1.0': {}}}}
    # breakpoint()
    assert expected == npm_dependencies.dependency_tree(package=p0)


def test_find_dependencies(packages, fake_db, monkeypatch):
    """
    Test a Package's dependencies, already within the DB, is found.
    """
    monkeypatch.setattr(npm_dependencies, 'DB', fake_db)
    _, p1, p2, p3, *__ = packages
    assert [p2, p3] == npm_dependencies.find_dependencies(p1, cached_only=True)


@responses.activate
def test_npm_repository():
    """
    Test we take a valid Response and return the list of dependencies.
    """
    # A quick mock solution for Responses thanks to
    # https://stackoverflow.com/a/23914464
    # as I can't directly create a Response object to mock requests's return
    url = "https://registry.npmjs.org/async/2.0.1"
    answer = {'name': 'async', 'dependencies': {'not_empty': '1.0'}}
    responses.add(responses.GET, url, json=answer, status=200)

    p = models.Package(name='async', version='2.0.1')
    assert npm_dependencies.npm_repository(p)


@pytest.mark.npm_requests
def test_npm_request():
    """
    Test tat we get the expected structure from NPM package repo.
    Namely, {'name': 'package_name', 'dependencies': {'dep_name': 'ver'}}
    """
    p = models.Package(name='async', version='2.0.1')
    res = npm_dependencies.npm_request(p)
    assert res.status_code == 200  # Until the day this package x version dies
    res_dict = res.json()
    assert res_dict
    assert 'name' in res_dict
    assert 'dependencies' in res_dict
    assert type(res_dict['dependencies']) is dict
