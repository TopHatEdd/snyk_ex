#!/usr/bin/env python3
"""
Test our utils module. Adapting the Package types.
"""
import json
import requests
import responses
from app import utils, models


def test_package2json():
    """
    Test a Package converts to JSON.
    """
    p = models.Package(name='pack', version='1.0')
    expected = json.dumps({'name': 'pack', 'version': '1.0'})
    assert utils.package2json(p) == expected


def test_json2package():
    """
    Test a JSON converts into Package.
    """
    json_raw = json.dumps({'name': 'another', 'version': '1.0'})
    expected = models.Package(name='another', version='1.0')
    assert expected == utils.json2package(json_raw)


@responses.activate
def test_response2packages():
    """
    Test we take a valid Response and return the list of dependencies.
    """
    # A quick mock solution for Responses thanks to
    # https://stackoverflow.com/a/23914464
    # as I can't directly create a Response object to mock requests's return
    url = "https://unreal.com"
    answer = {'name': '', 'dependencies': {'pack': '1.0'}}
    responses.add(responses.GET, url, json=answer, status=200)

    fake_res = requests.get(url)
    p = models.Package(name='async', version='2.0.1')
    expected = [models.Package(name='pack', version='1.0')]
    assert utils.response2packages(fake_res) == expected
